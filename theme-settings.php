<?php
/**
 * add some Solid settings to the base theme settings form.
 *
 * @param $form
 * @param $form_state
 */
function solid_form_system_theme_settings_alter(&$form, $form_state) {

  list($css_files_drupal, $js_files_drupal) = _solid_build_js_css_list();

  $form['solid_info'] = array(
    '#type' => 'container',
    '#prefix' => '&#1109;&#963;&#8467;&#953;&#8706; settings',
    '#weight' => -25
  );


   $form['solid_info']['js'] = array(
    '#type' => 'fieldset',
    '#title' => t('Javascripts'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Modify the javascript cruft killer')
  );


    $form['solid_info']['js']['solid_remove_contrib_jquery'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove jquery from core, so you can load our own in the theme?'),
      '#default_value' => theme_get_setting('solid_remove_contrib_jquery')
    );

  $form['solid_info']['js']['solid_remove_core_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove .js files from drupal misc.'),
    '#default_value' => theme_get_setting('solid_remove_core_js')
  );

  $form['solid_info']['js']['solid_remove_contrib_js'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove .js files from contrib modules'),
    '#default_value' => theme_get_setting('solid_remove_contrib_js')
  );


  $form['solid_info']['js']['solid_remove_misc_js'] = array(
    '#type' => 'fieldset',
    '#title' => t('Possibly stripping !files files', array('!files' => sizeof($js_files_drupal))),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['solid_info']['js']['solid_remove_misc_js']['info'] = array(
    '#type' => 'markup',
    '#title' => t('Path to the JS files that can become killed with above settings'),
    '#suffix' => implode('<br> ', $js_files_drupal)
  );

  $form['solid_info']['css'] = array(
    '#type' => 'fieldset',
    '#title' => t('CSS files'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('Modify the CSS cruft killer')
  );
  $form['solid_info']['css']['solid_remove_core_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove .css files from drupal misc'),
    '#default_value' => theme_get_setting('solid_remove_core_css')
  );

  $form['solid_info']['css']['solid_remove_contrib_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove .css files from contrib modules'),
    '#default_value' => theme_get_setting('solid_remove_contrib_css')
  );


  $form['solid_info']['css']['solid_remove_misc_css'] = array(
    '#type' => 'fieldset',
    '#title' => t('Possibly stripping !files files', array('!files' => sizeof($css_files_drupal))),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['solid_info']['css']['solid_remove_misc_css']['info'] = array(
    '#type' => 'markup',
    '#title' => t('Path to the CSS files thats gonna be stripped '),
    '#suffix' => implode('<br> ', $css_files_drupal)
  );
}


function _solid_build_js_css_list() {

  $result = db_query("SELECT * FROM {system} WHERE type = 'module' AND status = 1");
  $js_files_drupal = array();
  $css_files_drupal = array();
  foreach ($result as $module) {
    $module_path = pathinfo($module->filename, PATHINFO_DIRNAME);
    $js_files = file_scan_directory($module_path, '/.*\.js$/');
    $css_files = file_scan_directory($module_path, '/.*\.css$/');

    foreach ((array) $js_files as $key => $file) {
      $js_files_drupal[] = $module_path . "/" . $file->filename;
    }

    foreach ((array) $css_files as $key => $file) {
      $css_files_drupal[] = $module_path . "/" . $file->filename;
    }
  }
  $js_misc_files = file_scan_directory('misc', '/.*\.js$/');
  foreach ((array) $js_misc_files as $key => $file) {
    $js_files_drupal[] = 'misc' . "/" . $file->filename;
  }

  asort($js_files_drupal);
  asort($css_files_drupal);
  return array($css_files_drupal, $js_files_drupal);
}